
var imgIds = ["dayDigit1", "dayDigit2", "monthDigit1", "monthDigit2", "yearDigit1", "yearDigit2", 
    "yearDigit3", "yearDigit4", "hourDigit1", "hourDigit2", "minuteDigit1", "minuteDigit2", 
    "secondDigit1", "secondDigit2"];
var allDigits = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];

function start(){
    updateAllDigits();

    var oneSecond = 1000;
    setInterval(updateAllDigits, oneSecond);
}

function updateAllDigits(){
    var date = new Date();

    getNumber(date.getDate(), "dayDigit", 0);
    getNumber(date.getMonth()+1, "monthDigit", 2);
    getYear(date.getFullYear());

    getNumber(date.getHours(), "hourDigit", 8);
    getNumber(date.getMinutes(), "minuteDigit", 10);
    getNumber(date.getSeconds(), "secondDigit", 12);

    updateClock();
}

function updateClock(){
    for(var i = 0; i < imgIds.length; i++){
        var src = document.getElementById(imgIds[i]).src;
        if(src == "" || !src.endsWith("/images/" + allDigits[i] + ".png")){
            document.getElementById(imgIds[i]).src = "images/" + allDigits[i] + ".png";
        }
    }
}

function getYear(year){
    var digits = year.toString().split("");
    for(var i = 1; i <= digits.length; i++){
        allDigits[i+3] = digits[i-1];
    }
}

function getNumber(number, id, index){
    var digit1, digit2;
    var digits = number.toString().split("");
    if(digits.length > 1){
        digit1 = digits[0];
        digit2 = digits[1];
    }else{
        digit1 = '0';
        digit2 = digits[0];
    }
    allDigits[index] = digit1;
    allDigits[index+1] = digit2;
}
